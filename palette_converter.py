from typing import Set, Tuple
import argparse
from PIL import Image

Color3 = Tuple[int, int, int]

def color_distance(color_1: Color3, color_2: Color3) -> float:
  """Calculates perception-weighted distance between two colors, i.e. how "similar" two colors are.
  Lower is better.

  Args:
      color_1 (Color3): first color
      color_2 (Color3): second color

  Returns:
      float: perception-weighted distance
  """
  r1, g1, b1 = color_1
  r2, g2, b2 = color_2
  dist = ((r2 - r1) * 0.30) ** 2 + \
         ((g2 - g1) * 0.59) ** 2 + \
         ((b2 - b1) * 0.11) ** 2
  return dist

def get_best_color(palette: Set[Color3], color_to_fit: Color3) -> Color3:
  """Given a palette and a sample color, selects the color in the palette
  that most closely resembles the sample color.

  Args:
      palette (Set[Color3]): The palette containing all valid colors.
      color_to_fit (Color3): The sample color.

  Returns:
      Color3: the closest palette color.
  """
  min_d = float("inf")
  best_color = None
  for pcolor in palette:
    d = color_distance(color_to_fit, pcolor)
    if d < min_d:
      min_d = d
      best_color = pcolor
  return best_color

def convert_image(source_path: str, target_path: str, output_path: str):
  try:
    palette_image = Image.open(source_path).convert('RGB')
  except Exception as e:
    raise ValueError("Failed to open path. Does this path point to a valid image? %s : %s" % (source_path, str(e)))

  try:
    target_image = Image.open(target_path).convert('RGB')
  except Exception as e:
    raise ValueError("Failed to open path. Does this path point to a valid image? %s : %s" % (target_path, str(e)))

  # parse palette
  bg_color = palette_image.getpixel((0, 0))
  palette = set()
  x, y = palette_image.size
  for i in range(x):
    for j in range(y):
      col = palette_image.getpixel((i, j))
      if not col == bg_color:
        palette.add(col)

  x, y = target_image.size
  for i in range(x):
    for j in range(y):
      col = target_image.getpixel((i, j))
      if not col == bg_color:
        best_color = get_best_color(palette, col)
        target_image.putpixel((i, j), best_color)
      else:
        target_image.putpixel((i, j), bg_color)
  target_image.save(output_path)

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Convert image using the color palette of the other image.')
  parser.add_argument('source_file', type=str, help='the source file whose palette we will use. The top left \
                                                     pixel must contain the unused background color. The image \
                                                     should be in RGB format with no alpha.')
  parser.add_argument('target_file', type=str, help='the file we wish to convert. Make sure to use the same \
                                                     background color as the source file. The image \
                                                     should be in RGB format with no alpha')
  parser.add_argument('-o', '--output', type=str, default='conv.png', help='the filename of the output.')

  args = parser.parse_args()
  output_file = args.output
  source_file = args.source_file
  target_file = args.target_file

  convert_image(source_file, target_file, output_file)