# Usage

`pip install -r requirements.txt` to install PIL, which is required for this script.

`python palette_converter.py -h` will display the usage.

# Credits

`default_palette.png` is a modification of `{Ereshkigal} v1.0.png` from the FE Repo, made by Ereshkigal.